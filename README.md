## **PRESENTACIÓN DEL TALLER 0:** APRENDE GIT CON GATITOS :kissing_cat:


:stars: :stars: :stars:
Aquí está incluida la presentación del taller de la charla de el Puente en Madrid
que trata sobre el **sistema de versiones git** y de la creación de una **pequeña web**
el **24 de octubre a las 19:00 horas hasta las 21:00 horas** hora española.